;;; -*- Mode: Lisp -*-

;;; letv-package.lisp
;;;
;;; See file COPYING for copyright and licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.CL.EXTENSIONS.LETV" (:use "CL")
  (:nicknames "LETV")
  (:export "LETV" "LETV*")
  (:documentation
   "The LETV Package.

Exports two macros, LETV and LETV* that allow to combine standard LET
and LET* constucts with MULTIPLE-VALUE-BIND in a possible less verbose
way that also requires less indentation.")
  )

;;; end of file -- letv-package.lisp

