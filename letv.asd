;;; -*- Mode: Lisp -*-

;;; letv.asd
;;;
;;; See file COPYING for copyright and licensing information.

(asdf:defsystem "letv"
  :description
  "The LETV Package.

Exports two macros, LETV and LETV* that allow to combine standard LET
and LET* constucts with MULTIPLE-VALUE-BIND in a possible less verbose
way that also requires less indentation."

  :author "Marco Antoniotti"
  :license "BSD"

  :components ((:file "letv-package")
               (:file "letv" :depends-on ("letv-package")))
  )

(eval-when (:load-toplevel :compile-toplevel :execute)
  (pushnew :letv *features*))

;;; end of file -- letv.asd
