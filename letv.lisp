;;; -*- Mode: Lisp -*-

;;; letv.lisp
;;;
;;; See file COPYING for copyright and licensing information.
;;;
;;; Quite complicated after all, given all the corner cases involving
;;; blocks, return and type declarations, w.r.t., the semantics of
;;; M-V-B, LET and LET*.
;;;
;;; The syntax of LETV (or LETV*) is very "loopy", with a nod to
;;; SML/OCaml/F#/Haskell/Julia.
;;;
#|

	letv	 ::= 'LETV' [vars] [IN [body]]
	letvstar ::= 'LETV*' [vars] [IN [body]]

	vars	 ::= var | var vars

	var	 ::= ids '=' <form> [decls]

        decls    ::= decl | decl decls
        decl     ::= OF-TYPE idtypes | DECLARE <declaration>

	ids	 ::= <symbol> | '(' <symbol> + ')'
	idtypes	 ::= <type designator> | '(' <type designator> + ')'

	body	 ::= [<declarations>] <form> *

|#
;;;
;;; Notes:
;;; 2023-03-09: The DECLARE declaration is not handled yet.


;;; TODO:
;;; 1. Fix parser to handle the DECLARE options and simplify the
;;;    TYPE-SPECS handling.


(in-package "LETV")

;;; parse-letv-vars-n-body
;;; Separates the variable specs from the body of the LETV/LETV* form.

(defun parse-letv-vars-n-body (vars-n-body)

  (declare (type list vars-n-body))

  ;; A very kludgy and hatchet job.

  ;; We return two values: the "vars" and the "body"

  (let* ((body (member "IN" vars-n-body
                       :key #'(lambda (e) (when (symbolp e) e)) 
                       :test #'string-equal))
         (vars (ldiff vars-n-body body))
         )
    (values vars (rest body))
    ))


;;; parse-letv-vars
;;; Parses the variables specifications for a LETV/LETV* form.

(defun parse-letv-vars (vars-specs
                        &aux vars forms type-specs declarations)

  (declare (type list vars-spec)
           (type list vars forms type-specs declarations)
           (ignore declarations))

  (labels ((start (vs)
             (parse-vars vs))

           (parse-vars (vs)
             (if (null vs)
                 (finish)
               (destructuring-bind (v eqsign val &rest more-vars)
                   vs
                 (unless (string-equal "=" eqsign)
                   (error "LETV/*: malformed variable declaration..."))
                 (push v vars)
                 (push val forms)
                 (if more-vars
                     (cond ((and (symbolp (first more-vars))
                                 (string-equal "OF-TYPE"
                                               (first more-vars)))
                            (push (second more-vars) type-specs)
                            (parse-vars (cddr more-vars)))

                           #| Unused FTTB
                           ((and (symbolp (first more-vars))
                                 (string-equal "DECLARE"
                                               (first more-vars)))
                            (push (second more-vars) declarations)
                            (parse-vars (cddr more-vars)))
                           |#

                           (t
                            (push nil type-specs)
                            (parse-vars more-vars)
                            ))
                   (progn
                     (push nil type-specs)
                     (finish)))
                 )))

           #| Unused FTTB
           (parse-of-type (type-and-more)
             (unless type-and-more ; Must begin with a <type spec>
               (error "LETV/*: nothing after OF-TYPE."))
             (push (first type-and-more) type-specs)
             (let ((next-token (second type-and-more)))
               (cond ((and (symbolp next-token)
                           (string-equal "OF-TYPE"))
                      (error "LETV/*: duplicate OF-TYPE declaration."))

                     ((and (symbolp next-token)
                           (string-equal "DECLARE"))
                      (parse-declare (cddr type-and-more)))

                     (t
                      (parse-vars (cddr type-and-more)))
                     ))
             )

           (parse-declare (decl-and-more)
             (unless decl-and-more ; Must begin with a <type spec>
               (error "LETV/*: nothing after DECLARE."))
             (push (first decl-and-more) declarations)
             (let ((next-token (second decl-and-more)))
               (cond ((and (symbolp next-token)
                           (string-equal "OF-TYPE"))
                      (parse-of-type (cddr decl-and-more)))

                     ((and (symbolp next-token)
                           (string-equal "DECLARE"))
                      (error "LETV/*: duplicate DECLARE qualifier."))

                     (t
                      (parse-vars (cddr type-and-more)))
                     ))
             )
           |#

           (finish ()
             (values (nreverse vars)
                     (nreverse forms)
                     (nreverse type-specs)))
           )
    (start vars-specs)))


;;; build-par-binding-forms
;;; Builds the LET that is the translation of a LETV form.

(defun build-par-binding-forms (vars forms type-specs body)
  (if (null vars)
      `(progn ,@body)
      (loop for v in vars
            and f in forms
            and ts in type-specs
            collect v into vbs
            collect f into vfs
            if (symbolp v)
              when ts collect `(type ,ts ,v) into decls end
            else
              if (listp v)
                if (and ts (listp ts))
                  append (mapcan #'(lambda (mvv mvt)
                                     (when mvt
                                       `((type ,mvt ,mvv))))
                                 v
                                 ts)
                    into decls
                else
                  if (and ts (not (listp ts)))
                    do (error "LETV/*: malformed type spec ~S in multiple-value spec."
                              ts)
                    end
                  end
              else
                do (error "LETV/*: malformed variable(s) ~S." v)
            end
            finally
              (return
               `(let ,(collect-vars vbs)
                  ,@(if decls
                        `((locally (declare ,@decls)
                            ,@(build-setq-stmts vbs vfs)
                            ,@body))
                        `(,@(build-setq-stmts vbs vfs)
                          ,@body))
                  )
               ))
      ))


;;; collect-vars
;;; Given the variable "bindings" of a LETV/LETV* form it "flattens"
;;; it. It also checks that every variable is actually a symbol and
;;; generated a TYPE-ERROR if it is not.

(declaim (ftype (function (list) list) collect-vars))

(defun collect-vars (vars)
  (declare (type list vars))
  (loop for v in vars
        if (symbolp v)
          collect v
        else
          if (listp v)
            append v
          else do
              (error 'type-error
                     :datum v
                     :expected-type '(or symbol list))
        end
        ))


;;; check-vars
;;; Traverses and collect duplicates in a list of variable "bindings"
;;; and throws and error if there are any.

(declaim (ftype (function (list) list) check-vars))

(defun check-vars (vars)
  ;; VARS comes from PARSE-LETV-VARS, therefore it is a list of
  ;; symbols and lists of symbols.  A TYPE-ERROR is thrown by
  ;; COLLECT-VARS if a strange element is in the list.
  ;;
  ;; Simple, quadratic member-based loop.  The assumption is that we
  ;; never have very long lists. Change to a HASH-TABLE if it gets too
  ;; slow.

  (declare (type list vars))

  (let ((vs (collect-vars vars)))
    (declare (type list vs))

    (loop for (v . more-vs) of-type (symbol . list) on vs
          when (member v more-vs :test #'eq)
            collect v into duplicates
          finally
            (when duplicates
              (error "LETV/*: duplicate variable~P~{ ~A~^,~} in LETV/* form."
                     (length duplicates)
                     duplicates))
          )
    vs
    ))


;;; build-setq-stmts
;;; Builds the interleaved sequence of PSETQs and MULTIPLE-VALUE-SETQs
;;; that initialize the variables introduced by LETV.

(defun build-setq-stmts (vars forms)
  ;; VARS and FORMS have the same length.
  ;; VARS contains symbols and lists o symbols (variables and multiple-values).

  (declare (type list vars forms))

  (labels ((collect-setqs (vs fs collected-setqs)
             (if (null vs)
                 collected-setqs ; Done.
                 (etypecase (first vs)
                   (symbol (collect-symbol-vars (rest vs)
                                                (rest fs)
                                                collected-setqs
                                                `(psetq ,(first vs) ,(first fs))
                                                ))

                   (list (collect-mv-vars (rest vs)
                                          (rest fs)
                                          collected-setqs
                                          `(multiple-value-setq
                                               ,(first vs)
                                               ,(first fs))
                                          ))
                   ))
             )

           (collect-symbol-vars (vs fs collected-setqs sv-form)
             (if (null vs)
                 (append collected-setqs (list sv-form))
                 (etypecase (first vs)
                   (symbol (collect-symbol-vars (rest vs)
                                                (rest fs)
                                                collected-setqs
                                                (append sv-form
                                                        (list (first vs)
                                                              (first fs)))))

                   (list (collect-setqs vs
                                       fs
                                       (append collected-setqs (list sv-form))
                                       ))
                   )))

           (collect-mv-vars (vs fs collected-setqs mv-form)
             (if (null vs)
                 (append collected-setqs (list mv-form))
                 (etypecase (first vs)
                   (symbol (collect-setqs vs
                                         fs
                                         (append collected-setqs
                                                 (list mv-form))))
                   (list (collect-setqs (rest vs)
                                       (rest fs)
                                       (append collected-setqs
                                               (list mv-form)
                                               (list
                                                `(multiple-value-setq
                                                     ,(first vs)
                                                     ,(first fs))))))
                   )))
           )
    (check-vars vars)
    (collect-setqs vars forms ())
    ))


(defmacro letv (&body vars-n-body)
  "Expands in a LET where each variable is initialized in a subsequent step.

The macro introduces a less verbose way to introduce bindings while
adopting a LOOP-like syntax.

Each variable is initialized by means of PSETQs and
MULTIPLE-VALUE-SETQs.  Care is taken to mimic the semantic of LET and
to properly handle type declarations by means of LOCALLY.
"
  (multiple-value-bind (vars body)
      (parse-letv-vars-n-body vars-n-body)
    (if vars
        (multiple-value-bind (vars forms type-specs)
            (parse-letv-vars vars)
          (build-par-binding-forms vars forms type-specs body)
          )
        `(block nil ,@body))
    ))


;;; build-seq-binding-forms
;;; Builds the nested sequence of LET* and M-V-Bs that are the
;;; expansion of a LETV form.

(defun build-seq-binding-forms (vars forms type-specs body)

  (declare (type list vars forms type-specs))

  (if (null vars)
      body
      (loop with mvb-form = nil
            for (v . more-vars) on vars
            and (f . more-forms) on forms
            and (ts . more-type-specs) on type-specs
            if (symbolp v)
              collect v into vs
              and collect f into fs
              and when ts collect (list 'type ts v) into decls end
            else
              do (setq mvb-form
                       `(multiple-value-bind ,v
                            ,f
                          ,@(when (and ts (some #'identity ts))
                              `((declare ,@(loop for var in v
                                                 and vart in ts
                                                 when ts
                                                   collect (list 'type vart var)
                                                   ))))
                          ,@(if more-vars
                                (list
                                 (build-seq-binding-forms more-vars
                                                          more-forms
                                                          more-type-specs
                                                          body))
                                body))
                       )
              and do (loop-finish)
            end
            finally
              (cond ((and vs mvb-form)
                     (return
                      `(let* ,(mapcar #'list vs fs)
                         ,@(when decls
                             `((declare ,@decls)))
                         ,mvb-form)))
                    (vs
                     (return
                      `(let* ,(mapcar #'list vs fs)
                         ,@(when decls
                             `((declare ,@decls)))
                         ,@body)))

                    (mvb-form
                     (return mvb-form))

                    (t
                     (return `(progn)))
                    )
            )))


(defmacro letv* (&body vars-n-body)
  "Expands in a nested structure of LET*s and MULTIPLE-VALUE-BINDs.

The macro introduces a less verbose way to introduce bindings while
adopting a LOOP-like syntax."
  (multiple-value-bind (vars body)
      (parse-letv-vars-n-body vars-n-body)
    (if vars
        (multiple-value-bind (vars forms type-specs)
            (parse-letv-vars vars)
          (build-seq-binding-forms vars forms type-specs body)
          )
        `(block nil ,@body))
    ))


;;; end of file -- letv.lisp

